import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * JFrame important methods
 * .add(content)
 * .pack()
 * .setPrefferedSize()
 * .setVisible()
 * .setDefaultCloseOperation(Jframe EXIT_ON_CLOSE)
 * 
 * public class Grid extends JPanel
 * 
 */


public class Main {

	public static void main(String[] args) {
		drawWindow();
		
	}
	
	public static void drawWindow(){
		JFrame frame = new JFrame("Frame");
		Grid g = new Grid();
		g.setPreferredSize(new Dimension (1280, 720));
		frame.add(g);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public class Grid extends JPanel{
		private Cell[][]cell = new Cell[20][20];
		@Override
		public void paint(Graphics g) {
			g.drawRect(10, 10, 20, 20);
		}
	}
	
	public class Cell {
		
	}
}